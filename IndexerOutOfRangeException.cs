﻿using System;
using System.Runtime.Serialization;

namespace PracticeForTest2
{
    [Serializable]
    internal class IndexerOutOfRangeException : Exception
    {
        public IndexerOutOfRangeException()
        {
        }

        public IndexerOutOfRangeException(string message) : base(message)
        {
        }

        public IndexerOutOfRangeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IndexerOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}