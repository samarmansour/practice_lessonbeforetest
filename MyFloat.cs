﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeForTest2
{
    class MyFloat
    {
        public int IntegerPart { get; set; }
        public int FractionalPart { get; set; }
        public bool Positive { get; set; }

        // Check if the giving number is positive or not -- overloading operator
        public static bool IsPositive(double num)
        {
            if (num < 0)
                throw new NegativeNotSupportedException($"Cannot have a negavite number, {num}");
            return true;
        }

        // Two functions to Split double into two parts --- 2305.063
        // SplitFirstPartDouble --> 2305
        // SplitFractionalPartDouble --> 063
        public static int SplitFirstPartDouble(double num)
        {
            
            string[] numToString = num.ToString().Split('.');
            
            int numIntegerPart = int.Parse(numToString[0]);
            return numIntegerPart;
        }

        public static int SplitFractionalPartDouble(double num)
        {
            string[] numToString = num.ToString().Split('.');
            int numFactionalPart = int.Parse(numToString[1]);
            return numFactionalPart;
        }
        public MyFloat(int integerPart, int fractionalPart, bool positive)
        {
            if (integerPart < 0)
                throw new NegativeNotSupportedException($"Cannot have a negavite number, {integerPart}");
            IntegerPart = integerPart;
            if (fractionalPart < 0)
                throw new NegativeNotSupportedException($"Cannot have a negavite number, {fractionalPart}");
            FractionalPart = fractionalPart;
            if (!positive)
                throw new NegativeNotSupportedException($"Cannot have a negavite number");
            Positive = positive;
        }

        public static MyFloat operator +(MyFloat myFloat, double num)
        {
            MyFloat result = new MyFloat(myFloat.IntegerPart + SplitFirstPartDouble(num), myFloat.FractionalPart + SplitFractionalPartDouble(num), myFloat.Positive && IsPositive(num));
            return result;
        }

        public static MyFloat operator +(MyFloat myFloat, MyFloat myFloat2)
        {
            MyFloat fl = new MyFloat(myFloat.IntegerPart + myFloat2.IntegerPart, myFloat.FractionalPart + myFloat2.FractionalPart, myFloat.Positive && myFloat2.Positive);
            return fl;
        }

        public static MyFloat operator -(MyFloat myFloat, double num)
        {
            MyFloat result = new MyFloat(myFloat.IntegerPart - SplitFirstPartDouble(num), myFloat.FractionalPart - SplitFractionalPartDouble(num), myFloat.Positive && IsPositive(num));
            return result;
        }

        public static MyFloat operator *(MyFloat myFloat, double num)
        {
            MyFloat result = new MyFloat(myFloat.IntegerPart * SplitFirstPartDouble(num), myFloat.FractionalPart * SplitFractionalPartDouble(num), myFloat.Positive && IsPositive(num));
            return result;
        }
        public static MyFloat operator /(MyFloat myFloat, double num)
        {
            MyFloat result = new MyFloat(myFloat.IntegerPart / SplitFirstPartDouble(num), myFloat.FractionalPart / SplitFractionalPartDouble(num), myFloat.Positive && IsPositive(num));
            return result;
        }

        public static MyFloat operator ==(MyFloat myFloat, double num)
        {
            IsPositive(num);
            if (!(myFloat.IntegerPart == SplitFirstPartDouble(num) && myFloat.FractionalPart == SplitFractionalPartDouble(num)))
            {
                return new MyFloat(0,0,false);
            }
            return new MyFloat(myFloat.IntegerPart, myFloat.FractionalPart, myFloat.Positive);
        }

        public static MyFloat operator !=(MyFloat myFloat, double num)
        {
            return myFloat == num;
        }

        public int this[int index]
        {
            get
            {
                if (IntegerPart + FractionalPart < index)
                    throw new IndexerOutOfRangeException($"{index} number is larger than digits of {IntegerPart + FractionalPart}");
                return index;
            }
        }

        public override string ToString()
        {
            return $"Number {IntegerPart}.{FractionalPart} IsPositive: {Positive}";
        }
    }
}
