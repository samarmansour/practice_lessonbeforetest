﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeForTest2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyFloat fl = new MyFloat(12345, 5462, true);
            MyFloat fl2 = new MyFloat(4523, 5, true);
            //MyFloat fl3 = new MyFloat(562, -56, false); throws NegativeNotSupportedException 

            Console.WriteLine(fl);
            Console.WriteLine(fl + fl2);
            Console.WriteLine(fl + 253.1);
            Console.WriteLine(fl * 2.2);
            Console.WriteLine(fl / 3.2);
            Console.WriteLine(fl - 5.4);

            Console.WriteLine(fl != fl2);

            //Console.WriteLine(fl[256352]); //throws IndexerOutOfRangeExceptionan  
            Console.WriteLine(fl[5]);


        }
    }
}
