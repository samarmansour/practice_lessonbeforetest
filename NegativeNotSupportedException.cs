﻿using System;
using System.Runtime.Serialization;

namespace PracticeForTest2
{
    [Serializable]
    internal class NegativeNotSupportedException : Exception
    {
        public NegativeNotSupportedException()
        {
        }

        public NegativeNotSupportedException(string message) : base(message)
        {
        }

        public NegativeNotSupportedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NegativeNotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}